class Fixnum

  def in_words
    result = ""
    num = self
    if self < 20
      return titles_hash[self]
    elsif self < 100 && self % 10 == 0
      return titles_hash[self]
    elsif self < 1000
      return number_words_under_1000(self)
    end
  end




  def titles_hash
    titles_hash = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety"
    }
  end

  def number_words_under_1000(num)
    #given a three digit number, return the english word equivalent
    result = []
    ones = num % 10
    tens = (num % 100) - ones
    hundreds = num / 100

    if hundreds > 0
      result.push(titles_hash[hundreds])
      result.push("hundred")
    end

    if tens == 10
      arg = tens + ones
      result.push(titles_hash[(arg)])
    elsif tens > 0 && tens != 1
      result.push(titles_hash[tens])
    end

    if ones > 0 && tens != 10
      result.push(titles_hash[ones])
    end

    result.join(" ")
  end

end
